import CommandArgsValidator from './input-validator';
import WhiteListLogValidation from './white-list-validation';

export {
    CommandArgsValidator,
    WhiteListLogValidation
}