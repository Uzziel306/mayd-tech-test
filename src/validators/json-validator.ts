export default class JsonValidator {
    static validate(jsonToParse: string) {
        try {
            const jsonParsed =  JSON.parse(jsonToParse);
            return jsonParsed
        } catch (error) {
            throw new Error(`JSON parse failed with this string ${jsonToParse}`)
        }
    }
}