export default class LineValidator {
    static validate(logLine: string) {
        const regexValidator = /(.*)( - )([a-zA-Z]*)( - )({[^]*?})/s;

        if (!regexValidator.test(logLine)){
            throw new Error(`logLine "${logLine}" does not match with the regex validator`);
        }
        return true;
    }
}