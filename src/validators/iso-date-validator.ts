import { IsoDate } from "../utils";

export default class IsoDateValidator {
    static validate(isoDate: string) {
        const regexValidator = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)((-(\d{2}):(\d{2})|Z)?)/s;

        if (!regexValidator.test(isoDate)){
            throw new Error(`isoDate "${isoDate} does not match with regex validator"`)
        }

        return IsoDate.toTimestamp(new Date(isoDate));
    }
}