import logType from "../types/log/log-type";
import whiteList from "../conf/allowed-logs";
export default class WhiteListLogValidation {
    static isAllowed(type: logType) {
        return whiteList.includes(type);
    }
}