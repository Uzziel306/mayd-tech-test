import logType from "../types/log/log-type";

export default class LogLevelValidator {
    static validate(logLevel: string) {
        const regexValidator = /^[a-zA-Z]+/s;

        if (!regexValidator.test(logLevel)) {
            throw new Error(`logLevel: "${logLevel}" does not match with regex validator`);
        }

        const logLevelEnum = logType[logLevel as keyof typeof logType];
        if (!logLevelEnum) {
            throw new Error(`logType: "${logLevel}" not implement`)
        }

        return logLevelEnum;
    }
}