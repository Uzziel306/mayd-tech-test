import { ICommandArgsValidatorArgs } from '../types/input-validator'

export default class CommandArgsValidator {
    static validate(fileBuffer: ICommandArgsValidatorArgs) {
        if (!(fileBuffer.input && fileBuffer.output)) {
            throw new Error('Bad input arguments, expected receive "input" or "output" params')
        }
        return true;
    }
}