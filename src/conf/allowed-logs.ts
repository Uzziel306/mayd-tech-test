import logType from "../types/log/log-type";

// add log types to the white list to allow them into the parser
const whiteList: logType[] = [
    logType.error,
    // logType.info
];

export default whiteList;