import ILogStructure from "../types/log/log-structure";
import LineValidator from "../validators/line-validator";
import IsoDateValidator from "../validators/iso-date-validator";
import LogLevelValidator from "../validators/log-level-validator";
import JsonValidator from "../validators/json-validator";

export default class LogValues{
    static parseToJson(logLine: string): ILogStructure{
        LineValidator.validate(logLine);

        const [isoDate, logLevel, additionalInfoJson] = logLine.split(' - ');

        // validations per field
        const timestamp = IsoDateValidator.validate(isoDate)
        const logLevelEnum = LogLevelValidator.validate(logLevel);
        const jsonParsed = JsonValidator.validate(additionalInfoJson);

        return {
            timestamp,
            logLevel: logLevelEnum,
            additional: jsonParsed,
        }
    }
}