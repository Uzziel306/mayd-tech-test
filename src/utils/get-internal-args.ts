const skipBaseArgumentsIndex = 2;

export default class CommandArgs{
    static get() {
        return require('minimist')(process.argv.slice(skipBaseArgumentsIndex));
    }
}