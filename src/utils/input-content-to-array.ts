export default class InputContentToArray{
    static parse(inputContent: string){
        return inputContent.split("\n")
        // removing empty lines
        .filter(logLine => logLine.length);
    }
}