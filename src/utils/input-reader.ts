import fs from 'fs';

const encoding = 'utf8'

export default class InputReader {
    static getInputContent(inputPath: string): string {
        return fs.readFileSync(inputPath, encoding);
    }
}