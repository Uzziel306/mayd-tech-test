import fs from 'fs'

export default class OutputWriter {
    static write(outputPath: string, outputContent: any[]) {
        return fs.writeFileSync(outputPath, JSON.stringify(outputContent));
    }
}