export default class IsoDate{
    static toTimestamp(date: Date){
        return date.getTime();
    }
}