import Main from "./main";

const index = () => {
    const mainLauncher = new Main();
    mainLauncher.init();
}

index();