import Log from './types/log/log';
import { CommandArgs, InputReader, InputContentToArray, LogValues, OutputWriter } from './utils';
import { CommandArgsValidator, WhiteListLogValidation } from './validators';

export default class Main {
    init() {
        const args = CommandArgs.get();
        CommandArgsValidator.validate({ input: args.input, output: args.output });

        const inputContent = InputReader.getInputContent(args.input);
        const logArray = InputContentToArray.parse(inputContent);

        const outputCollector = [];
        for (const logLine of logArray) {
            try {
                const logData = LogValues.parseToJson(logLine);

                if (!WhiteListLogValidation.isAllowed(logData.logLevel)) {
                    continue;
                }

                outputCollector.push(new Log(logData).getParse())
            } catch (error) {
                console.error(`Skipping log line: "${logLine}" for error: ${error}`);
            }
        }

        OutputWriter.write(args.output, outputCollector);
    }
}
