import Main from "../main";
import fs from 'fs';

describe('App testing', () => {
    const filePath = 'errors.json';

    beforeAll(() => {
        if (fs.existsSync(filePath)) {
            fs.unlinkSync(filePath)
        }
    })

    afterAll(() => {
        if (fs.existsSync(filePath)) {
            fs.unlinkSync(filePath)
        }
    })
    test('basic example from Code challenge', () => {
        // adding command line args
        process.argv.push('--input');
        process.argv.push('./app.log');

        process.argv.push('--output');
        process.argv.push('./errors.json');

        const mainLauncher = new Main();
        mainLauncher.init();

        const fileContent = String(fs.readFileSync(filePath, 'utf8'));
        const expected = '[{"timestamp":1628475171259,"logLevel":"error","transactionId":"9abc55b2-807b-4361-9dbe-aa88b1b2e978","err":"Not found"}]'

        expect(fileContent).toEqual(expected);
    })
})