import LineValidator from '../../validators/line-validator'

describe('Line validator', () => {
    test('valid line to parse', () => {
        const validLine = '2044-08-09T02:12:51.253Z - info - {"transactionId":"9abc55b2-807b-4361-9dbe-aa88b1b2e978","details":"Service is started"}';

        expect(LineValidator.validate(validLine)).toEqual(true)
    })

    test('invalid line to parse', () => {
        const invalidLine = '2044-08-09T - info -';

        expect(() => LineValidator.validate(invalidLine)).toThrowError()
    })
})