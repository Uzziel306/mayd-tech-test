import IsoDateValidator from '../../validators/iso-date-validator'

describe('ISO date validator', () => {
    test('Valid ISO date parse', () => {
        const validIsoDate = '2044-08-09T02:12:51.253Z';
        const unixTimeStampValue = 2354321571253;

        expect(IsoDateValidator.validate(validIsoDate)).toEqual(unixTimeStampValue)
    })

    test('Bad ISO date format', () => {
        const invalidIsoDate = 'hello there';

        expect(() => IsoDateValidator.validate(invalidIsoDate)).toThrowError()
    })
})