import CommandArgsValidator from '../../validators/input-validator'

describe('Command Args Validator', () => {
    test('exist input and output fields', () => {
        const input = './app.log';
        const output = './errors.json';

        expect(CommandArgsValidator.validate({
            input, output
        })).toEqual(true)
    })

    test('not exist input field', () => {
        const input = '';
        const output = './errors.json';

        expect(() => CommandArgsValidator.validate({
            input, output
        })).toThrow(Error)
    })

    test('not exist output field', () => {
        const input = './app.log';
        const output = '';

        expect(() => CommandArgsValidator.validate({
            input, output
        })).toThrow(Error)
    })
})