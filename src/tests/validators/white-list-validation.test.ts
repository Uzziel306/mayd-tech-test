import WhiteListLogValidation from '../../validators/white-list-validation'
import logType from '../../types/log/log-type'

describe('White list validator', () => {
    test('valid log type in white list', () => {
        const validLogType= logType.error;

        expect(WhiteListLogValidation.isAllowed(validLogType)).toEqual(true)
    })

    test('invalid log type in white list', () => {
        const invalidLogType = logType.info;

        expect(WhiteListLogValidation.isAllowed(invalidLogType)).toEqual(false);
    })
})