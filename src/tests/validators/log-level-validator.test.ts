import LogLevelValidator from '../../validators/log-level-validator'
import logType from '../../types/log/log-type'

describe('Log level validator', () => {
    test('valid log level to validate', () => {
        const validLogLevel = 'info';

        expect(LogLevelValidator.validate(validLogLevel)).toEqual(logType.info)
    })

    test('invalid log level to validate', () => {
        const invalidLogLevel = 'kitty';

        expect(() => LogLevelValidator.validate(invalidLogLevel)).toThrowError(`logType: "${invalidLogLevel}" not implement`);
    })

    test('invalid log level to validate in regex', () => {
        const invalidLogLevel = '1234';

        expect(() => LogLevelValidator.validate(invalidLogLevel)).toThrowError(`logLevel: "${invalidLogLevel}" does not match with regex validator`);
    })
})