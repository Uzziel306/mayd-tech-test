import JsonValidator from '../../validators/json-validator'

describe('JSON parser validator', () => {
    test('valid JSON to parse', () => {
        const validStringJson = '{"transactionId":"9abc55b2-807b-4361-9dbe-aa88b1b2e978","details":"Service is started"}';
        const jsonToExpect = { "transactionId": "9abc55b2-807b-4361-9dbe-aa88b1b2e978", "details": "Service is started" };

        expect(JsonValidator.validate(validStringJson)).toEqual(jsonToExpect)
    })

    test('invalid JSON to parse', () => {
        const validStringJson = '{"transactionId"tarted"}';

        expect(() => JsonValidator.validate(validStringJson)).toThrowError()
    })
})