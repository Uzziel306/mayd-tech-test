import toTimestamp from '../../utils/iso-date'

describe('ISO date util', () => {
    test('Valid Date to parse to timestamp', () => {
        const validIsoDate = new Date('2044-08-09T02:12:51.253Z');
        const unixTimeStampValue = 2354321571253;

        expect(toTimestamp.toTimestamp(validIsoDate)).toEqual(unixTimeStampValue)
    })
})