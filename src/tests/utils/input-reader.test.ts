import InputReader from '../../utils/input-reader'
import fs from 'fs';

describe('InputReader util', () => {
    const filePath = 'temp-input-reader.txt';
    const fileContent = 'Hello there!';

    beforeAll(() => {
        fs.writeFileSync(filePath, fileContent)
    })

    afterAll(() => {
        if (fs.existsSync(filePath)) {
            fs.unlinkSync(filePath)
        }
    })

    test('Read a existed file ', () => {
        expect(InputReader.getInputContent(filePath)).toEqual(fileContent)
    })
})