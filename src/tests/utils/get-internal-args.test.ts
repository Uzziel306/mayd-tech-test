import CommandArgs from '../../utils/get-internal-args';

describe('LogValues util', () => {
    test('valid log', () => {
        const expected = {input: './app.log', output: './errors.json'};

        // adding command line args
        process.argv.push('--input');
        process.argv.push('./app.log');

        process.argv.push('--output');
        process.argv.push('./errors.json');

        expect(CommandArgs.get())
        .toMatchObject(expected)
    });
})