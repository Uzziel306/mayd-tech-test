import LogValues from '../../utils/get-log-values';

describe('LogValues util', () => {
    test('valid log', () => {
        const validLogLine = '2021-08-09T02:12:51.259Z - error - {"transactionId":"9abc55b2-807b-4361-9dbe-aa88b1b2e978","details":"Cannot find user orders list","code": 404,"err":"Not found"}';
        const expectJsonParsed = {
            timestamp: 1628475171259,
            logLevel: 'error',
            additional: {
              transactionId: '9abc55b2-807b-4361-9dbe-aa88b1b2e978',
              details: 'Cannot find user orders list',
              code: 404,
              err: 'Not found'
            }
          }
        expect(LogValues.parseToJson(validLogLine)).toEqual(expectJsonParsed)
    });
})