import OutputWriter from '../../utils/output-writer'
import fs from 'fs';

describe('OutputWriter util', () => {
    const filePath = 'temp.txt';
    beforeAll(() => {
        if (fs.existsSync(filePath)) {
            fs.unlinkSync(filePath)
        }
    })

    afterAll(() => {
        if (fs.existsSync(filePath)) {
            fs.unlinkSync(filePath)
        }
    })

    test('write a new file', () => {
        const fileContent = 'Hello there!';


        OutputWriter.write(filePath, [fileContent]);

        expect(fs.existsSync(filePath)).toEqual(true)
    })
})