import InputContentToArray from '../../utils/input-content-to-array'

describe('InputContentToArray util', () => {
    test('single line', () => {
        const singleLine = '2044-08-09T02:12:51.253Z - info - {"transactionId":"9abc55b2-807b-4361-9dbe-aa88b1b2e978","details":"Service is started"}';
        const expectedArray = ['2044-08-09T02:12:51.253Z - info - {"transactionId":"9abc55b2-807b-4361-9dbe-aa88b1b2e978","details":"Service is started"}'];

        expect(InputContentToArray.parse(singleLine)).toEqual(expectedArray)
    })

    test('multiple lines with empty new line', () => {
        const singleLine = '2044-08-09T02:12:51.253Z - info - {"transactionId":"9abc55b2-807b-4361-9dbe-aa88b1b2e978","details":"Service is started"}\n2044-08-09T02:12:51.253Z - info - {"transactionId":"9abc55b2-807b-4361-9dbe-aa88b1b2e978","details":"Service is started"}\n';
        const expectedArray = ['2044-08-09T02:12:51.253Z - info - {"transactionId":"9abc55b2-807b-4361-9dbe-aa88b1b2e978","details":"Service is started"}', '2044-08-09T02:12:51.253Z - info - {"transactionId":"9abc55b2-807b-4361-9dbe-aa88b1b2e978","details":"Service is started"}'];

        expect(InputContentToArray.parse(singleLine)).toEqual(expectedArray)
    })
})