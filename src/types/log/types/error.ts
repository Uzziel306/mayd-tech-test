import ILog from '../log-interface'
import ILogStructure from '../log-structure'
import IErrorLogOutput from '../outputs/error'

export default class Error implements ILog {
    parse(logLine: ILogStructure): IErrorLogOutput {
        return {
            timestamp: logLine.timestamp,
            logLevel: logLine.logLevel,
            transactionId: logLine.additional?.transactionId,
            err: logLine.additional?.err
        }
    }
}