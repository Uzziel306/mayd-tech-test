import ILog from '../log-interface'
import ILogStructure from '../log-structure'
import IDefaultLogOutput from '../outputs/default'

export default class Info implements ILog {
    parse(logLine: ILogStructure): IDefaultLogOutput {
        return {
            logLevel: logLine.logLevel,
            timestamp: logLine.timestamp,
            transactionId: logLine.additional?.transactionId
        }
    }
}