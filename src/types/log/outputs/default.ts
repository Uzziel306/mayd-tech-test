import logType from '../log-type'

interface IDefaultLogOutput{
    timestamp: number,
    logLevel: logType,
    transactionId: string
}

export default IDefaultLogOutput;