import IDefaultLogOutput from "./default";

interface IErrorLogOutput extends IDefaultLogOutput{
    err: string
}

export default IErrorLogOutput;