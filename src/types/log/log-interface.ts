import ILogStructure from './log-structure';

export default interface ILog{
    parse(log: ILogStructure): any
}