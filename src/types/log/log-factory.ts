import ILog from "./log-interface";
import logType from "./log-type";
import Debug from "./types/debug";
import ErrorLog from "./types/error";
import Info from './types/info'
import Warn from "./types/warn";

export default class LogFactory {
    static createLogType(type: logType): ILog{
        if (type === logType.info) {
            return new Info()
        }
        else if (type === logType.warn) {
            return new Warn();
        }
        else if (type === logType.debug) {
            return new Debug();
        }
        else if (type === logType.error) {
            return new ErrorLog();
        }

        throw new Error(`logType: "${type}" not implement in factory class`)
    }
}