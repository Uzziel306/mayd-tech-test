import LogFactory from "./log-factory";
import ILog from "./log-interface";
import ILogStructure from "./log-structure";
import logType from "./log-type";

export default class Log{
    logType?: ILog;
    constructor(public logData: ILogStructure){}

    public getParse(){
        this.logType = LogFactory.createLogType(this.logData.logLevel);
        return this.logType.parse(this.logData)
    }
}