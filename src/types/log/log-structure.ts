import logType from "./log-type";

export default interface ILogStructure{
    timestamp: number,
    logLevel: logType,
    additional: any
}