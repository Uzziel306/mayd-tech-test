enum logType{
    info = "info",
    debug= "debug",
    error= "error",
    warn = "warn"
}

export default logType